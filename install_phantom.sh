#!/bin/bash

PHANTOM_VERSION="2.1.1"
PHANTOM_FILE="phantomjs-${PHANTOM_VERSION}-linux-x86_64"
PHANTOM_URL="https://bitbucket.org/ariya/phantomjs/downloads/${PHANTOM_FILE}.tar.bz2"

# install required packages
apt-get update
apt-get install -y build-essential chrpath libssl-dev libxft-dev
apt-get install -y libfreetype6 libfreetype6-dev
apt-get install -y libfontconfig1 libfontconfig1-dev

# install phantom js
cd /tmp
wget "${PHANTOM_URL}"
tar xvjf "${PHANTOM_FILE}.tar.bz2"
mv "${PHANTOM_FILE}" /usr/local/share
rm "${PHANTOM_FILE}.tar.bz2"

cd /usr/local/bin
ln -s "/usr/local/share/${PHANTOM_FILE}/bin/phantomjs"
