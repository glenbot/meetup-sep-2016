from distutils.core import setup
import os


def pip_requirements(extra=None):
    if extra is None:
        requirements_path = 'requirements.pip'
    else:
        requirements_path = 'requirements.{}.pip'.format(extra)
    with open(requirements_path) as f:
        return f.readlines()
    return []


setup(
    name='steamnotify',
    version='0.0.1',
    description='Notify of daily sales on steam',
    author='Glen Zangirolami',
    url='https://bitbucket.org/glenbot/meetup-sep-2016',
    py_modules=[
        'steamnotify',
        'steamnotify.tasks'
    ],
    install_requires=pip_requirements()
)
