import os
import csv
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

import luigi

from ..browser import BaseBrowser


class GetSteamDailyDeals(luigi.Task):
    url = luigi.Parameter()
    target_path = luigi.Parameter()

    def output(self):
        return luigi.LocalTarget(os.path.join(self.target_path, 'data', 'daily-steam-deals'))

    def run(self):
        deals = []

        # open the url in selenium
        browser = BaseBrowser(url=self.url)
        browser.open_url()

        # parse the daily deals
        games = browser.driver.find_elements_by_xpath('//*[@id="sales-section-daily-deal"]/table/tbody/tr')
        for game in games:
            game_attrs = game.find_elements_by_tag_name('td')
            link = game_attrs[0].find_element_by_tag_name('a').get_attribute('href')
            name = game_attrs[2].find_element_by_tag_name('a').text
            discount = game_attrs[3].text
            price = game_attrs[4].text
            ends = game_attrs[5].text
            rating = game_attrs[6].find_element_by_tag_name('span').text
            deals.append([link, name, discount, price, ends, rating])

        with self.output().open('w') as f:
            csv_writer = csv.writer(f)
            csv_writer.writerows(deals)


class EmailSteamDailyDeals(luigi.Task):
    server = luigi.Parameter()
    port = luigi.Parameter()
    username = luigi.Parameter()
    password = luigi.Parameter()
    subject = luigi.Parameter()
    to_email = luigi.Parameter()
    from_email = luigi.Parameter()

    def requires(self):
        return GetSteamDailyDeals()

    def run(self):
        html_message = []

        # setup the mime message
        message = MIMEMultipart('alternative')
        message['Subject'] = self.subject
        message['From'] = self.from_email
        message['To'] = self.to_email

        # connect to smtp server
        smtp = smtplib.SMTP_SSL(timeout=5)
        smtp.set_debuglevel(1)
        smtp.connect(self.server, int(self.port))
        smtp.login(self.username, self.password)

        # setup message body
        html_message.append('Todays Daily Deals')
        html_message.append('<table cellpadding="5" cellspacing="5" height="100%" width="100%" align="left" style="border: 1px solid #555; border-collapse: collapse;"><tr><th>name</th><th>Link</th><th>discount</th><th>price</th><th>ends</th><th>rating</th></tr>')
        with self.input().open('r') as f:
            csv_reader = csv.reader(f)
            for game in csv_reader:
                link, name, discount, price, ends, rating = game
                html_message.append('<tr><td>{}</td><td><a href="{}">{}</a></td><td>{}</td><td>{}</td><td>{}</td><td>{}</td></tr>'.format(
                    name,
                    link,
                    name,
                    discount,
                    price,
                    ends,
                    rating
                ))
        html_message.append('</table>')

        # send email
        part1 = MIMEText(''.join(html_message), 'html')
        message.attach(part1)
        smtp.sendmail(self.from_email, self.to_email, message.as_string())
        smtp.quit()
