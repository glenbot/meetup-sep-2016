import logging
import time

from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import NoSuchElementException

logger = logging.getLogger(__name__)


class BaseBrowser:
    def __init__(self, window_size=(1920, 1080), url=None):
        self._driver = None
        self.width, self.height = window_size
        self.logger = logger

        if url is not None:
            self.url = url

    @property
    def driver(self):
        if self._driver is None:
            self._driver = webdriver.PhantomJS()
            self._driver.set_window_size(self.width, self.height)
        return self._driver

    def open_url(self, url=None):
        if url is None:
            url = self.url
        logger.info('Opening URL %s', url)
        self.driver.get(url)
        time.sleep(5)

    def quit(self):
        self.driver.quit()
